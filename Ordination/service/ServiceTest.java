package service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runners.model.Annotatable;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class ServiceTest {

	private DagligSkaev ds;
	private DagligFast df;
	private PN pn;
	private Service service;
	private Patient pt;
	private Laegemiddel lm;
	private Storage storage;
	
	
	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		service.createSomeObjects();
		pt = new Patient ("1234", "Bo", 110);
		lm = new Laegemiddel("cancer", 1.2, 1.3, 1.3, "stk");		
		LocalTime tid[] = {LocalTime.of(16, 00), LocalTime.of(17, 00), LocalTime.of(18, 00)};
		double antal[] = {1, 2, 3};
		lm = new Laegemiddel("AIDS", 10, 11, 12, "kg");
		pt = new Patient("242424-2424", "HITLER", 200);
		ds = service.opretDagligSkaevOrdination((LocalDate.of(2016, 1, 10)),LocalDate.of(2016, 1, 11), pt, lm, tid, antal);
		df = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 19),pt,lm, 4, 4, 4, 4);
		pt.addOrdination(ds);
		pn = service.opretPNOrdination(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 19), pt, lm, 10);
	}

	@Test
	public void testAfSkaev() {
		LocalTime tid[] = {LocalTime.of(16, 00), LocalTime.of(17, 00), LocalTime.of(18, 00)};
		double antal[] = {1, 2, 3};
		service.opretDagligSkaevOrdination((LocalDate.of(2016, 1, 10)),LocalDate.of(2016, 1, 11), pt, lm, tid, antal);


	}
	@Test
	public void testAfFast() {
		df = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 19),pt,lm, 4, 4, 4, 4);
	}


	@Test
	public void testAfPN() {
		pn = service.opretPNOrdination(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 19), pt, lm, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testError1(){
		service.ordinationPNAnvendt(pn, LocalDate.of(2016, 3, 1));
	}

}


