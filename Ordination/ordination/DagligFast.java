package ordination;

import java.time.*;

public class DagligFast extends Ordination {
	private Dosis[] doser = new Dosis[4];
	private String type = "Daglig Fast";
	
	public DagligFast(LocalDate startDate, LocalDate slutDate, double morgenAntal, double middagAntal, double aftenAntal, double natAntal){

		super(startDate, slutDate);
		doser[0] = new Dosis(LocalTime.of(8, 0), morgenAntal);
		doser[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
		doser[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
		doser[3] = new Dosis(LocalTime.of(22, 0), natAntal);
	}
	
	@Override
	public double samletDosis() {
		return doegnDosis()*antalDage();
	}
	
	
	@Override
	public double doegnDosis() {
		double dosis = 0;
		for (int i = 0; i < doser.length; i++) {
			dosis += doser[i].getAntal();
		}
		return dosis;
	}

	@Override
	public String getType() {
		return type;
	}

	public Dosis[] getDoser() {
		return doser;
	}
    

	
}
