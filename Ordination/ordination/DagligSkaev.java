package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<>();
	private double antal;
	private String type = "Daglig Skeav";
	private LocalTime tid;


	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis d1 = new Dosis(tid, antal);
		doser.add(d1);
	}
/*
 * (non-Javadoc)
 * @see ordination.Ordination#samletDosis()
 * 
 */
	@Override
	public double samletDosis() {
		return doegnDosis()*antalDage();
	}

	@Override
	public double doegnDosis() {
		if(doser.size() == 0) {
			return 0;
		}
		double dosis = 0;
		for (int i = 0; i < doser.size(); i++) {
			dosis += doser.get(i).getAntal();
			
		}
		return dosis;
	}

	@Override
	public String getType() {

		return type;
	}


	public double getAntal() {
	
			return antal;
	
	}

	public void setAntal(double antal) {
		this.antal = antal;
	}

	public LocalTime getTid() {
		return tid;
	}

	public void setTid(LocalTime tid) {
		this.tid = tid;
	}

	public ArrayList<Dosis> getDoser(){
		return new ArrayList<>(doser);
	}
	
}
