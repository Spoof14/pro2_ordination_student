package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import service.Service;

public class DagligSkaevTest {

	private DagligSkaev daglig;
	private DagligSkaev daglig2;
	private Service service;
	private Patient pt;
	private Laegemiddel lm;
	private DagligSkaev daglig3;

	
	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();

		
		lm = new Laegemiddel("AIDS", 10, 11, 12, "kg");
		pt = new Patient("242424-2424", "HITLER", 200);
	
		
	}

	@Test
	public void testSamletDosis() {
		daglig = new DagligSkaev(LocalDate.of(2016, 1, 10), LocalDate.of(2016, 1, 11));
		daglig.opretDosis(LocalTime.of(16, 00), 5000);
		daglig.opretDosis(LocalTime.of(17, 00), 5000);
		daglig.opretDosis(LocalTime.of(18, 00), 5000);
		daglig.setLaegemiddel(lm);
		assertEquals(30000, daglig.samletDosis(),0.001);
	}
	
	@Test
	public void testSamletDosis2() {
		daglig2 = new DagligSkaev(LocalDate.of(2016, 1, 10), LocalDate.of(2016, 1, 11));
		daglig2.opretDosis(LocalTime.of(13, 00), 5);
		daglig2.opretDosis(LocalTime.of(14, 00), 5);
		daglig2.opretDosis(LocalTime.of(15, 00), 5);
		daglig2.setLaegemiddel(lm);
		assertEquals(30, daglig2.samletDosis(),0.001);
	}
	
	@Test
	public void testDoegnDosis() {
		daglig = new DagligSkaev(LocalDate.of(2016, 1, 10), LocalDate.of(2016, 1, 11));
		daglig.opretDosis(LocalTime.of(16, 00), 5000);
		daglig.opretDosis(LocalTime.of(17, 00), 5000);
		daglig.opretDosis(LocalTime.of(18, 00), 5000);
		daglig.setLaegemiddel(lm);
		assertEquals(15000, daglig.doegnDosis(),0.001);
	}
	
	@Test
	public void testDoegnDosis2() {
		daglig2 = new DagligSkaev(LocalDate.of(2016, 1, 10), LocalDate.of(2016, 1, 11));
		daglig2.opretDosis(LocalTime.of(13, 00), 5);
		daglig2.opretDosis(LocalTime.of(14, 00), 5);
		daglig2.opretDosis(LocalTime.of(15, 00), 5);
		daglig2.setLaegemiddel(lm);

		assertEquals(15, daglig2.doegnDosis(),0.001);
	}
	
	@Test
	public void testOpretDosis() {
		daglig3 = new DagligSkaev(LocalDate.of(2016, 1, 10), LocalDate.of(2016, 1, 11));
		daglig3.opretDosis(LocalTime.of(13, 00), 5);
		daglig3.opretDosis(LocalTime.of(15, 00), 502);
		
		Dosis dos = new Dosis(LocalTime.of(13, 00), 5);
		Dosis dos2 = new Dosis(LocalTime.of(15, 00), 502);
		
		ArrayList<Dosis> liste = new ArrayList<>();
		
		liste.add(dos);
		liste.add(dos2);
		
		for(int i = 0; i< liste.size(); i++) {
			assertEquals(liste.get(i).getTid(), daglig3.getDoser().get(i).getTid());
			assertEquals(liste.get(i).getAntal(), daglig3.getDoser().get(i).getAntal(), 0.001);
		}
		
		
	}	
	
	
	@Test(expected = IllegalArgumentException.class)
	public void testError(){
		double antal2[] = {2, 3, 4};
		LocalTime tid3[] = {LocalTime.of(13, 00), LocalTime.of(14, 00), LocalTime.of(15, 00)};
		service.opretDagligSkaevOrdination((LocalDate.of(2016, 1, 10)),LocalDate.of(2016, 1, 9), pt, lm, tid3, antal2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testError2(){
		LocalTime tid2[] = {LocalTime.of(13, 00), LocalTime.of(14, 00), LocalTime.of(15, 00)};
		double antal3[] = {40000, 40000, 40000};
		service.opretDagligSkaevOrdination((LocalDate.of(2016, 1, 10)),LocalDate.of(2016, 1, 11), pt, lm, tid2, antal3);
	}

}
