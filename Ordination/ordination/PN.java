package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination{
	private ArrayList<LocalDate> pngives = new ArrayList<LocalDate>();
	private String type = "PN";


	public PN(LocalDate startDen, LocalDate slutDen, double antal) {
		super(startDen, slutDen);
		this.antalEnheder = antal;
	}


	private double antalEnheder;

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen
	 * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {

		//		if(givesDen == null) {
		//			return false;
		//		}
		if(givesDen.isAfter(getStartDen().minusDays(1))) {
			if(givesDen.isBefore(getSlutDen().plusDays(1))) {

				pngives.add(givesDen);
				return true;
			}
			return false;
		}
		else {

			return false;
		}

	}
	public double samletDosis() {

		return this.getAntalGangeGivet()*this.getAntalEnheder();
	}

	public double doegnDosis() {
		if(pngives.size() == 0) {
			return 0;
		}
		return samletDosis()/((int) ChronoUnit.DAYS.between(pngives.get(0), pngives.get(pngives.size()-1))+1);
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * @return
	 */
	public int getAntalGangeGivet() {
		return pngives.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {

		return type;
	}

}
