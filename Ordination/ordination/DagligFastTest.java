package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import service.Service;

public class DagligFastTest {
	private DagligFast daglig;
	private DagligFast daglig2;
	private DagligFast daglig3;
	private Service service;
	private Patient pt;
	private Laegemiddel lm;

	
	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		lm = new Laegemiddel("AIDS", 10, 11, 12, "kg");
		pt = new Patient("242424-2424", "HITLER", 200);
		daglig = new DagligFast(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 12), 4, 4, 4, 4);
		daglig2 = new DagligFast(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 12), 0, 0, 0, 1);
		daglig3 = new DagligFast(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 11), 400.4, 40.1, 400.5, 4000);
		daglig.setLaegemiddel(lm);
	}

	@Test
	public void testSamletDosis() {
		assertEquals(48, daglig.samletDosis(),0.001);
	}
	
	@Test
	public void testSamletDosis2() {
		assertEquals(3, daglig2.samletDosis(),0.001);
	}
	
	@Test
	public void testSamletDosis3() {
		assertEquals(9682, daglig3.samletDosis(),0.001);
	}
	
	@Test
	public void testDoegnDosis() {
		assertEquals(16, daglig.doegnDosis(),0.001);
	}
	
	@Test
	public void testDoegnDosis2() {
		assertEquals(1, daglig2.doegnDosis(),0.001);
	}
	
	@Test
	public void testDoegnDosis3() {
		assertEquals(4841, daglig3.doegnDosis(),0.001);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testError(){
		service.opretDagligFastOrdination(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 9),pt,lm, 4, 4, 4, 4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testError2(){
		service.opretDagligFastOrdination(LocalDate.of(2016, 1, 10),LocalDate.of(2016, 1, 9),pt,lm, 40000, 40000, 4, 4);
	}


}
