package ordination;

import java.util.ArrayList;

public class Patient {
    ArrayList<Ordination> ordi = new ArrayList<Ordination>();
	private String cprnr;
    private String navn;
    private double vaegt;


    public Patient(String cprnr, String navn, double vaegt) {
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
    }

    public String getCprnr() {
        return cprnr;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public double getVaegt(){
        return vaegt;
    }

    public void setVaegt(double vaegt){
        this.vaegt = vaegt;
    }

    public void addOrdination(Ordination ordination){
    	ordi.add(ordination);
    }
    
    public void setOrdination(int i, Ordination ordination){
    	ordi.set(i, ordination);
    }
    
    public Ordination getOrdination(int i){
    	return ordi.get(i);
    }
    
    public void removeOrdination(int i){
    	ordi.remove(i);
    }
    
    @Override
    public String toString(){
        return navn + "  " + cprnr;
    }

	public ArrayList<Ordination> getOrdinationer() {
		return new ArrayList<>(ordi);
	}

}
